My entry for Ludum Dare 46 (if it's ready).

### Building ###
#### On Linux ####
```shell
$ cd ld46
$ git clone https://jlm@bitbucket.org/jlm/lib2d.git  # Unless it's already installed
$ cmake -B build
$ cmake --build build
$ ./build/bluebird
```
#### With Emscritpen ####
This assumes that the file ~/p/emlibs/lib2d/release/liblib2d.a exists.
```shell
$ cd ld26
$ ./embuild
```

### Playing the Game ###
You are the bird in the center of the screen.
Use the left and right arrows to navigate.
Catch flies and bring them to the nest to feed your chicks.
Avoid hawks.

### Stuff I want to do ###
* Implement flocking.
* Statically compile assets.
* Make a tiny UI library using lib2d for future LD jams.
