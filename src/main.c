#include <stdio.h>
#include <SDL2/SDL.h>

#include <lib2d.h>
#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif // __EMSCRIPTEN__

#include "game.h"
#include "renderer.h"

int vp[2];
char result;

static bool running = false;
static SDL_Window* window;
static SDL_Event event;
static uint32_t frame_start;

#ifdef __EMSCRIPTEN__
EM_JS(int, get_canvas_width, (void), { return canvas.width; })
EM_JS(int, get_canvas_height, (void), { return canvas.height; })
EM_JS(void, get_canvas_size, (int* s), {
        s[0] = canvas.width;
        s[1] = canvas.height;
})
#endif /* __EMSCRIPTEN__ */

static void update_screen_size(int w, int h) {
    vp[0] = w;
    vp[1] = h;
    lib2d_viewport(w, h);
}

static void quit_loop() {
#ifdef __EMSCRIPTEN__
                emscripten_cancel_main_loop();
#else
                running = false;
#endif
}

static void loop() {
    while (SDL_PollEvent(&event)) {
        switch(event.type) {
            case SDL_QUIT:
                quit_loop();
                break;
            case SDL_WINDOWEVENT:
                if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                    update_screen_size(event.window.data1, event.window.data2);
                }
                break;
            case SDL_KEYUP:
                if (event.key.keysym.sym == SDLK_SPACE) {
                    result = 0;
                    game_new();
                } else if (event.key.keysym.sym == SDLK_ESCAPE) {
                    quit_loop();
                }
                break;
        }
        if (!result) {
            game_event(event);
        }
    }

    uint32_t now = SDL_GetTicks();
    uint32_t dt = now - frame_start;
    frame_start = now;

    result = game_step(dt * 0.001f);

    lib2d_clear(1.f, 1.f, 1.f, 1.f);
    render();
    lib2d_render();
    SDL_GL_SwapWindow(window);
}

int main(void) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        fprintf(stderr, "SDL_Init failed: %s\n", SDL_GetError());
        return -1;
    }


#ifdef __EMSCRIPTEN__
    get_canvas_size(vp);
#else
    SDL_DisplayMode DM;
    if (SDL_GetCurrentDisplayMode(0, &DM) == 0) {
        vp[0] = DM.w;
        vp[1] = DM.h;
    }
#endif

    window = SDL_CreateWindow("Bluebird",
            SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
            vp[0], vp[1], SDL_WINDOW_OPENGL|SDL_WINDOW_RESIZABLE);
    SDL_GL_CreateContext(window);

    if (lib2d_init(LIB2D_RENDER_BACKEND_GL, 0) != 0) {
        fprintf(stderr, "lib2d_init failed: %s\n", lib2d_get_error());
        return -1;
    }
    lib2d_viewport(vp[0], vp[1]);

    renderer_load_sprites();
    game_new();
    result = 0;

    running = true;
    frame_start = 0;
#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop(loop, 0, 1);
#else
    while (running) loop();
#endif
    renderer_unload_sprites();
    lib2d_shutdown();
    SDL_DestroyWindow(window);
    SDL_Quit();
    return 0;
}
