#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include "game.h"

#define MAX(a,b) ((a)?(b):(a)>(b))
#define MIN(a,b) ((a)?(b):(a)<(b))

#define in_map(a) (a >= 0 && a <= MAP_SIZE)

Gamestate gamestate;

char flies_in_nest;
extern char result;
static float time_going;

static bool collides(vec2 a, vec2 b, float radius) {
    vec2 d;
    vec2_sub(d, a, b);
    float m = vec2_mul_inner(d, d);
    return m < radius*radius;
}

static bool collides_aabb(vec2 a, vec2 b, float ra, float rb) {
    return !(a[0]-ra >= b[0]+rb ||
             a[0]+ra <= b[0]-rb ||
             a[1]-ra >= b[1]+rb ||
             a[1]+ra <= b[1]-rb);
}

static float randf() {
    return rand() / (float) RAND_MAX;
}

static void initalize_object(MovableObject* o) {
    o->pos[0] = randf() * MAP_SIZE;
    o->pos[1] = randf() * MAP_SIZE;
    float a = randf();
    o->velocity[0] = cosf(a);
    o->velocity[1] = sinf(a);
    o->turning = 0;
}

void game_new() {
    srand(time(0));
    time_going = 0;
    flies_in_nest = 0;

    gamestate.nest.pos[0] = MAP_SIZE / 2;
    gamestate.nest.pos[1] = MAP_SIZE / 2;
    gamestate.nest.health = 1;
    gamestate.nest.size = 1;

    for (int i=0; i<NUM_HAWKS; i++) {
        initalize_object(&gamestate.hawks[i]);
    }
    for (int i=0; i<NUM_FLIES; i++) {
        initalize_object(&gamestate.flies[i]);
    }
    for (int i=0; i<NUM_TREES; i++) {
        float* t = gamestate.trees[i];
        do {
            t[0] = roundf(randf() * MAP_SIZE);
            t[1] = roundf(randf() * MAP_SIZE);
        } while (collides(t, gamestate.nest.pos, 5));
    }

    vec2_dup(gamestate.bluebird.pos, gamestate.nest.pos);
    gamestate.bluebird.velocity[0] = 0;
    gamestate.bluebird.velocity[1] = -1;
    gamestate.bluebird.turning = 0;
    gamestate.bluebird_cargo = CARGO_TYPE_end;
}

static void wrap_axis(float* a) {
    if (*a < 0) {
        *a += MAP_SIZE;
    } else if (*a > MAP_SIZE) {
        *a -= MAP_SIZE;
    }
}

static void wrap_pos(float* vec) {
    wrap_axis(vec);
    wrap_axis(vec+1);
}

static void move(MovableObject* o, float turn_speed, float radius, float dt) {
    if (o->turning) {
        vec2_rot(o->velocity, o->velocity, turn_speed * o->turning * dt);
    }

    vec2 dist;
    vec2_scale(dist, o->velocity, dt);
    vec2_dup(o->old_pos, o->pos);

    o->pos[0] += dist[0];
    wrap_axis(o->pos); // Wrap x

    for (int i=0; i<NUM_TREES; i++) {
        float* t = gamestate.trees[i];
        if (collides_aabb(o->pos, t, radius, TREE_RADIUS)) {
            if (o->velocity[0] > 0) {
                o->pos[0] = t[0] - TREE_RADIUS - radius;
            } else {
                o->pos[0] = t[0] + TREE_RADIUS + radius;
            }
        }
    }
    o->pos[1] += dist[1];
    wrap_axis(o->pos + 1); // Wrap y
    for (int i=0; i<NUM_TREES; i++) {
        float* t = gamestate.trees[i];
        if (collides_aabb(o->pos, t, radius, TREE_RADIUS)) {
            if (o->velocity[1] > 0) o->pos[1] = t[1] - TREE_RADIUS - radius;
            else o->pos[1] = t[1] + TREE_RADIUS + radius;
        }
    }
}

int game_step(float dt) {
    if (result) return result;
    time_going += dt;

    vec2_norm(gamestate.bluebird.velocity, gamestate.bluebird.velocity);
    vec2_scale(gamestate.bluebird.velocity, gamestate.bluebird.velocity, 5.f);
    move(&gamestate.bluebird, M_PI, BLUEBIRD_RADIUS, dt);

    for (int i=0; i<NUM_HAWKS; i++) {
        MovableObject* o = &gamestate.hawks[i];

        if (o->is_chasing) {
            vec2 diff;
            vec2_sub(diff, o->pos, gamestate.bluebird.pos);
            vec2_norm(diff, diff);
            vec2_sub(diff, diff, o->velocity);
            float angle = atan2f(diff[1], diff[0]);
            if (angle > M_PI*2) {
                angle -= M_PI*2;
            } else if (angle < -M_PI*2) {
                angle += M_PI*2;
            }

            if (angle > 0.1f && angle < M_PI - 0.1f) {
                o->turning = -1;
            } else if (angle < 0.1f && angle > -M_PI+0.1f) {
                o->turning = 1;
            } else {
                o->turning = 0;
            }
        }
        vec2_norm(o->velocity, o->velocity);
        vec2_scale(o->velocity, o->velocity, 5.f);
        move(o, M_PI/2.f, HAWK_RADIUS, dt);
    }
    for (int i=0; i<NUM_FLIES; i++) {
        MovableObject* o = &gamestate.flies[i];

        vec2 dist;
        vec2_norm(o->velocity, o->velocity);
        vec2_scale(o->velocity, o->velocity, 6.f);
        vec2_scale(dist, o->velocity, dt);
        vec2_dup(o->old_pos, o->pos);

        o->pos[0] += dist[0];
        wrap_axis(o->pos);
        for (int j=0; j<NUM_TREES; j++) {
            float* t = gamestate.trees[j];
            if (collides_aabb(o->pos, t, FLY_RADIUS, TREE_RADIUS)) {
                o->pos[0] = o->old_pos[0];
                o->velocity[0] *= -1;
            }
        }
        o->pos[1] += dist[1];
        wrap_axis(o->pos+1);
        for (int j=0; j<NUM_TREES; j++) {
            float* t = gamestate.trees[j];
            if (collides_aabb(o->pos, t, BLUEBIRD_RADIUS, TREE_RADIUS)) {
                o->pos[1] = o->old_pos[1];
                o->velocity[1] *= -1;
            }
        }
    }

    gamestate.nest.health -= 0.02f * dt / 1000;
    if (gamestate.nest.health < 0) return -2;

    for (int i=0; i<NUM_HAWKS; i++) {
        if (collides(gamestate.bluebird.pos, gamestate.hawks[i].pos, 10)) {
            if (collides(gamestate.bluebird.pos, gamestate.hawks[i].pos,
                        BLUEBIRD_RADIUS + HAWK_RADIUS))
                return -1;
            else
                gamestate.hawks[i].is_chasing = true;
        } else if (gamestate.hawks[i].is_chasing) {
            gamestate.hawks[i].is_chasing = false;
        }
    }

    if (gamestate.bluebird_cargo == CARGO_TYPE_end) {
        for (int i=0; i<NUM_FLIES; i++) {
            if (collides(gamestate.bluebird.pos, gamestate.flies[i].pos,
                        BLUEBIRD_RADIUS + FLY_RADIUS)) {
                gamestate.flies[i].pos[0] = rand() % MAP_SIZE;
                gamestate.flies[i].pos[1] = rand() % MAP_SIZE;
                gamestate.bluebird_cargo = CARGO_TYPE_fly;
                break;
            }
        }
    } else if (collides(gamestate.bluebird.pos, gamestate.nest.pos,
                BLUEBIRD_RADIUS + NEST_RADIUS)) {
        gamestate.nest.health += 0.1f;
        flies_in_nest ++;
        gamestate.bluebird_cargo = CARGO_TYPE_end;
    }
    if (flies_in_nest >= FLIES_TO_WIN) return 1;

    return 0;
}

bool game_event(SDL_Event ev) {
    char* turning = &gamestate.bluebird.turning;
    if (ev.type == SDL_KEYDOWN) {
        if (ev.key.keysym.sym == SDLK_LEFT) {
            *turning = -1;
            return true;
        } else if (ev.key.keysym.sym == SDLK_RIGHT) {
            *turning = 1;
            return true;
        }
    } else if (ev.type == SDL_KEYUP) {
        if ((ev.key.keysym.sym == SDLK_LEFT && *turning == -1) ||
                (ev.key.keysym.sym == SDLK_RIGHT && *turning == 1)) {
            *turning = 0;
            return true;
        }
    }
    return false;
}
