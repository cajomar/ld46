#include <stdio.h>
#include <lib2d.h>

#include "renderer.h"
#include "game.h"

#define TILE_SIZE 64


lib2d_drawable nest_sprite;
lib2d_drawable bluebird_sprite;
lib2d_drawable bluebird_with_fly_sprite;
lib2d_drawable hawk_sprite;
lib2d_drawable fly_sprite;
lib2d_drawable ground_sprite;
lib2d_drawable tree_sprite;
lib2d_drawable gradient;
lib2d_font font;

extern int vp[2];
extern Gamestate gamestate;
extern char result;
extern char flies_in_nest;

static float scroll_x;
static float scroll_y;


static void load_image(lib2d_drawable* d, char* path, lib2d_image_info* info) {
    d->image = lib2d_image_load(path, info);
    if (!d->image)
        fprintf(stderr, "Couldn't load image %s: %s\n", path, lib2d_get_error());
    d->w = info->w;
    d->h = info->h;
    d->color = 0xffffffff;
}

void renderer_load_sprites(void) {
    lib2d_image_info info;

    load_image(&nest_sprite, "assets/nest.png", &info);
    nest_sprite.w = NEST_RADIUS*2*TILE_SIZE;
    nest_sprite.h = nest_sprite.w;

    load_image(&bluebird_sprite, "assets/bluebird.png", &info);
    bluebird_sprite.w = BLUEBIRD_RADIUS*2*TILE_SIZE;
    bluebird_sprite.h = bluebird_sprite.w;

    load_image(&bluebird_with_fly_sprite, "assets/bluebird_with_fly.png", &info);
    bluebird_with_fly_sprite.w = bluebird_sprite.w;
    bluebird_with_fly_sprite.h = bluebird_sprite.h;

    load_image(&hawk_sprite, "assets/hawk.png", &info);
    hawk_sprite.w = HAWK_RADIUS*2*TILE_SIZE;
    hawk_sprite.h = hawk_sprite.w;

    load_image(&fly_sprite, "assets/fly.png", &info);
    fly_sprite.w = FLY_RADIUS*2*TILE_SIZE;
    fly_sprite.h = fly_sprite.w;

    load_image(&ground_sprite, "assets/grass.png", &info);
    ground_sprite.w = TILE_SIZE * 1.8;
    ground_sprite.h = ground_sprite.w;

    load_image(&tree_sprite, "assets/tree.png", &info);
    tree_sprite.w = TREE_RADIUS * 2 * TILE_SIZE;
    tree_sprite.h = tree_sprite.w;

    load_image(&gradient, "assets/gradient.png", &info);

    if (lib2d_font_load("assets/Mali-Regular.ttf", &font) != 0) {
        fprintf(stderr, "Couldn't load font: %s\n", lib2d_get_error());
    }
}

void renderer_unload_sprites(void) {
    lib2d_image_delete(nest_sprite.image);
    lib2d_image_delete(bluebird_sprite.image);
    lib2d_image_delete(bluebird_with_fly_sprite.image);
    lib2d_image_delete(ground_sprite.image);
    lib2d_image_delete(tree_sprite.image);
    lib2d_image_delete(gradient.image);
    lib2d_font_delete(font);
}

static void to_pixel(float x, float y, float* x_, float* y_) {
    *x_ = x*TILE_SIZE - scroll_x;
    *y_ = y*TILE_SIZE - scroll_y;
}

static void center_on(lib2d_drawable* d, vec2 v) {
    to_pixel(v[0], v[1], &d->x, &d->y);
    d->x -= d->w / 2;
    d->y -= d->h / 2;
}

static bool in_viewport(vec2 v, float radius) {
    float px, py;
    to_pixel(v[0], v[1], &px, &py);
    radius *= TILE_SIZE;
    return (px > -radius && px < vp[0] + radius && py > -radius &&
            py < vp[1] + radius);
}

void render(void) {
    scroll_x = gamestate.bluebird.pos[0]*TILE_SIZE - vp[0]/2.f;
    scroll_y = gamestate.bluebird.pos[1]*TILE_SIZE - vp[1]/2.f;

    /*int left = scroll_x / TILE_SIZE - 2;*/
    /*int right = left + vp[0]/TILE_SIZE + 2;*/
    /*int top = scroll_y / TILE_SIZE - 1;*/
    /*int bottom = top + vp[1]/TILE_SIZE + 3;*/

    /*for (int y=top; y<bottom+1; y++) {*/
        /*for (int x=left; x<right+1; x++) {*/
            /*to_pixel(x, y, &ground_sprite.x, &ground_sprite.y);*/
            /*ground_sprite.rot = x + y;*/
            /*//ground_sprite.color = x+y*x;*/
            /*lib2d_draw(&ground_sprite);*/
        /*}*/
    /*}*/

    center_on(&nest_sprite, gamestate.nest.pos);
    lib2d_draw(&nest_sprite);

    center_on(&bluebird_sprite, gamestate.bluebird.pos);
    bluebird_sprite.rot = atan2f(gamestate.bluebird.velocity[1], gamestate.bluebird.velocity[0]);
    lib2d_draw(&bluebird_sprite);
    if (gamestate.bluebird_cargo == CARGO_TYPE_fly) {
        bluebird_with_fly_sprite.x = bluebird_sprite.x;
        bluebird_with_fly_sprite.y = bluebird_sprite.y;
        bluebird_with_fly_sprite.rot = bluebird_sprite.rot;
        lib2d_draw(&bluebird_with_fly_sprite);
    }

    for (int i=0; i<NUM_TREES; i++) {
        float* v = gamestate.trees[i];
        if (!in_viewport(v, TREE_RADIUS)) continue;
        center_on(&tree_sprite, v);
        lib2d_draw(&tree_sprite);
    }

    for (int i=0; i<NUM_HAWKS; i++) {
        MovableObject e = gamestate.hawks[i];
        if (!in_viewport(e.pos, HAWK_RADIUS)) continue;
        center_on(&hawk_sprite, e.pos);
        hawk_sprite.rot = atan2f(e.velocity[1], e.velocity[0]);
        lib2d_draw(&hawk_sprite);
    }
    for (int i=0; i<NUM_FLIES; i++) {
        MovableObject f = gamestate.flies[i];
        if (!in_viewport(f.pos, FLY_RADIUS)) continue;
        center_on(&fly_sprite, f.pos);
        fly_sprite.rot = atan2f(f.velocity[1], f.velocity[0]);
        lib2d_draw(&fly_sprite);
    }

    // Draw nest health backround
    lib2d_drawable d = {
        .w = vp[0]-64,
        .h = 64,
        .x = 32,
        .color = 0xaa8800ff,
    };
    int bar_top = vp[1] - 8 - d.h;
    d.y = bar_top;
    lib2d_draw(&d);

    lib2d_text(font, 20, "Chick Health:", 64, bar_top+20, 0xff);

    // Draw bar
    d.w = (vp[0]-128) * gamestate.nest.health;
    d.h = 4;
    d.x = 64;
    d.y = bar_top + 24;
    d.color = 0xff00ff;
    lib2d_draw(&d);

    lib2d_text(font, 20, "Chick size:", 64, bar_top+44, 0xff);

    d.w = (vp[0]-128) * ((flies_in_nest+1)/(FLIES_TO_WIN+1));
    d.y = bar_top + 48;
    lib2d_draw(&d);

    if (result == 1) {
        gradient.x = vp[0]/2 - gradient.w/2;
        gradient.y = vp[1]/2 - gradient.h/2;
        gradient.color = 0xffff00ff;
        lib2d_draw(&gradient);
        lib2d_text(font, 80, "You won!", gradient.x + 128,
                gradient.y + gradient.h/2, 0xff);
        lib2d_text(font, 38, "Your chick is able to fend for himself.",
                gradient.x, gradient.y + 208, 0xff);
        lib2d_text(font, 30, "Press the space bar to play again.",
                gradient.x + 72, gradient.y + 250, 0xff);
    } else if (result == -1) {
        gradient.x = vp[0]/2 - gradient.w/2;
        gradient.y = vp[1]/2 - gradient.h/2;
        gradient.color = 0xff0000ff;
        lib2d_draw(&gradient);
        lib2d_text(font, 80, "You lost!", gradient.x + 128,
                gradient.y + gradient.h/2, 0xff);
        lib2d_text(font, 40, "You were nabbed by a hawk.",
                gradient.x + 48, gradient.y + 208, 0xff);
        lib2d_text(font, 30, "Press the space bar to play again.",
                gradient.x + 72, gradient.y + 250, 0xff);
    } else if (result == -2) {
        gradient.x = vp[0]/2 - gradient.w/2;
        gradient.y = vp[1]/2 - gradient.h/2;
        gradient.color = 0xff0000ff;
        lib2d_draw(&gradient);
        lib2d_text(font, 80, "You lost!", gradient.x + 128,
                gradient.y + gradient.h/2, 0xff);
        lib2d_text(font, 40, "Your chick starved to death.", gradient.x + 48,
                gradient.y + 208, 0xff);
        lib2d_text(font, 30, "Press the space bar to play again.",
                gradient.x + 72, gradient.y + 250, 0xff);
    }
}
