#ifndef __BLUEBIRD_RENDERER__
#define __BLUEBIRD_RENDERER__

void renderer_load_sprites(void);
void renderer_unload_sprites(void);
void render(void);
#endif
