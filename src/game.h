#ifndef __BLUEBIRD_GAME__
#define __BLUEBIRD_GAME__

#include <stdbool.h>
#include <SDL2/SDL.h>

#include "mat.h"

#define HAWK_RADIUS .5
#define TREE_RADIUS .5
#define BLUEBIRD_RADIUS .25
#define FLY_RADIUS .125
#define NEST_RADIUS .5
#define NUM_HAWKS 50
#define NUM_FLIES 500
#define MAP_SIZE 100
#define NUM_TREES 1000

#define FLIES_TO_WIN 10.f

enum cargo_type {
    CARGO_TYPE_fly,
    CARGO_TYPE_end,
};

typedef struct movable_object {
    vec2 pos;
    vec2 old_pos;
    vec2 velocity;
    char turning;
    bool is_chasing;
} MovableObject;

typedef struct nest {
    vec2 pos;
    float health;
    float size;
} Nest;

typedef struct gamestate {
    MovableObject hawks[NUM_HAWKS];
    MovableObject flies[NUM_FLIES];
    vec2 trees[NUM_TREES];
    enum cargo_type bluebird_cargo;
    MovableObject bluebird;
    Nest nest;
} Gamestate;

void game_new();
int game_step(float dt);
bool game_event(SDL_Event ev);
#endif
